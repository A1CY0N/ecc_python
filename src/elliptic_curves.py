# /usr/bin/python3
# -*- coding: utf-8 -*-
"""
Authors and creation date
*************************
>>> #/usr/bin/python3
>>> @authors: A1CY0N - VC
>>> Created on Mon Mar 12 11:28:23 2018

*The example curve is y^2 = x^3 + x + 3 of order n = 17*

Main class
**********
"""
import sys
from ecc_utils import Utils as ecc_utils


class Ecc:
    """
        .. module:: elliptic_curves
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Simulate el gamal encryption

        Classe ``Ecc``
        ==============

        Simulate el gamal encryption

        :Example of package class import:

        >>> from ellipticCurves import Ecc
        >>> launch = Ecc.launch()
    """
    def __init__(self, number_bits, force=False):
        """
            This is the constructor of the Ecc class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: Ecc object
        """
        self._number_bits = number_bits
        curve_val_a, curve_val_b, mod = self.initialize_curve(force=force)
        self.ecc = ecc_utils(curve_val_a, curve_val_b, mod)
        self._point_p = self.ecc.point(3, 4)  # point_p = Generator point

    @property
    def number_bits(self):
        """
            _number_bits getter

            Args:
                :param self: Pending instance
                :type self: Ecc object
        """
        return self._number_bits

    @property
    def point_p(self):
        """
            _point_p getter

            Args:
                :param self: Pending instance
                :type self: Ecc object
        """
        return self._point_p

    def launch(self):
        """
            Launch El gamal encryption

            Args:
                :param self: Pending instance
                :type self: Ecc object

                :param force: force to defaults settings (no interactions)
                :type force: Boolean
        """
        # Diffie-Helmann
        point_q = self.ecc.random_public_point(self.ecc.mod, self.number_bits)
        print("------- Generation of a random point on the curve -------\n {}".format(point_q))
        two_p = self.ecc.double_point(self.point_p)
        # three_p = p_sum_q(two_p, point_p)
        four_p = self.ecc.double_point(two_p)
        print("-------- Check that the Q point is on the curve ---------\n {}".format(self.ecc.is_on_curve(point_q)))
        print("P + P = {}".format(two_p))
        print("4 * P = {}".format(four_p))

        print("4 * P with fast_exp = {}".format(self.ecc.fast_exp(self.point_p, 4)))
        print('\n')

        # Private keys generation
        alice_private_key = self.ecc.generate_private_key(self.number_bits)
        bob_private_key = self.ecc.generate_private_key(self.number_bits)
        print('The private keys are ALICE(%d), BOB(%d)' % (alice_private_key, bob_private_key))

        # Public keys generation
        alice_public_key = self.ecc.send_diffie_hellman(
            alice_private_key, self.point_p)
        bob_public_key = self.ecc.send_diffie_hellman(bob_private_key, self.point_p)

        if alice_public_key != self.ecc.origin and bob_public_key != self.ecc.origin:
            print('The public keys are Alice(%d, %d), Bob(%d, %d)\n' % (alice_public_key.x,
                                                                        alice_public_key.y,
                                                                        bob_public_key.x,
                                                                        bob_public_key.y))
        elif alice_public_key == self.ecc.origin:
            print('The public keys are Alice(%s), Bob(%d, %d)\n' % (alice_public_key,
                                                                    bob_public_key.x,
                                                                    bob_public_key.y))
        elif bob_public_key:
            print('The public keys are Alice(%d, %d), Bob(%s)\n' % (alice_public_key.x,
                                                                    alice_public_key.y,
                                                                    bob_public_key))
        else:
            print('The public keys are Alice(%s), Bob(%s)\n' % (alice_public_key, bob_public_key))

        # Shared secrets generation
        shared_secret_bob = self.ecc.receive_diffie_hellman(bob_private_key, alice_public_key)
        shared_secret_alice = self.ecc.receive_diffie_hellman(alice_private_key, bob_public_key)

        print("Bob's Shared Secret : {}".format(shared_secret_bob))
        print("Alice's Shared Secret : {}\n".format(shared_secret_alice))

        # El-Gamal encryption
        point_to_cipher = self.ecc.point(6, 2)
        point_c1, point_c2 = self.ecc.cipher(self.ecc.point(3, 4),
                                             alice_public_key,
                                             bob_private_key,
                                             point_to_cipher)

        print("The encrypted message is : {}, {}".format(point_c1, point_c2))
        print("The {} deciphered is {}".format(point_to_cipher, self.ecc.decipher(alice_private_key,
                                                                                  point_c1,
                                                                                  point_c2)))

    @staticmethod
    def initialize_curve(force=False):
        """
            **Function that initializes the curve

            Args:
                :param force: force to defaults settings (no interactions)
                :type force: Boolean

            Returns:
                :return: Returns A, B and the order of the curve.
                :rtype: int
        """
        if force:
            choice = 3
        else:
            print("Select mode :\n\n#1 Manual\n#2 Auto\n#3 Default\n")
            choice = int(input('> '))

        if choice == 1:  # Manuel
            print("Enter a :\n")
            initialize_manual_a = int(input())
            print("Enter b :\n")
            initialize_manual_b = int(input())
            print("Enter modulo :\n")
            initialize_manual_mod = int(input())
            return initialize_manual_a, initialize_manual_b, initialize_manual_mod

        if choice == 2:  # Auto
            # TO DO : generator
            initialize_auto_a = 1
            initialize_auto_b = 3
            initialize_auto_mod = 17
            return initialize_auto_a, initialize_auto_b, initialize_auto_mod

        if choice == 3:  # Default
            initialize_default_a = 1
            initialize_default_b = 3
            initialize_default_mod = 17
            return initialize_default_a, initialize_default_b, initialize_default_mod
        raise ValueError("Please enter a number between 1 and 3")

    def initialize_point(self):
        """
            **Function that initializes a point**

            Args:
                :param self: Pending instance
                :type self: Ecc object

            Returns:
                :return: point with initialized x and y values
                :rtype: point
        """
        print("Enter a point on the curve :\n")
        print("\nx = : ")
        initialize_x = int(input())
        print("\ny = : ")
        initialize_y = int(input())
        print("\nz = : ")
        if not isinstance(initialize_x, int) or not isinstance(initialize_y, int):
            raise ValueError('You have to enter integers')
        return self.ecc.point(initialize_x, initialize_y)


if __name__ == "__main__":
    FORCE = False
    if len(sys.argv) == 2:
        if sys.argv[1] == "-f":
            FORCE = True
    NUMBER_BITS = 8
    obj = Ecc(number_bits=NUMBER_BITS, force=FORCE)
    obj.launch()
