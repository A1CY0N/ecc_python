# -*- coding: utf-8 -*-
"""
>>> #/usr/bin/python3
>>> @authors: A1CY0N - VC
>>> Created on Mon Mar 12 11:28:23 2018

*The example curve is y^2 = x^3 + x + 3 of order n = 17*

Lists of available functions
****************************
"""
# Creation d'une classe de Point avec deux coordonnées.
from collections import namedtuple
import os
import sys
import string
import math


class Utils:
    """
        .. module:: ecc_utils
        .. moduleauthor:: A1CY0N <a1cy0n@tutanota.com>

        :platform: Unix, Windows
        :synopsis: Grouping crypto functions

        Classe ``Utils``
        ================

        Provides the necessary functions for El gamal encryption

        :Example of package class import:

        >>> from ecc_utils import Utils as ecc_utils
        >>> curve_val_a, curve_val_b, mod = self.initialize_curve(force=force)
        >>> ecc = ecc_utils(curve_val_a, curve_val_b, mod)
    """
    def __init__(self, curve_val_a, curve_val_b, mod):
        """
            This is the constructor of the Ecc class

            Configures class attributes

            Args:
                :param self: Pending instance
                :type self: Ecc object

                :param curve_val_a: Value of a
                :type curve_val_a: int

                :param curve_val_b: Value of b
                :type curve_val_b: int

                :param mod: Modulo value
                :type mod: int
        """
        self.point = namedtuple("Point", "x y")

        # Point at origin.
        self.origin = 'Origin'
        self.curve_val_a = curve_val_a
        self.curve_val_b = curve_val_b
        self.mod = mod

        # The example curve is y^2 = x^3 + x + 3 of order n = 17.
        # self.mod = 17
        # self.curve_val_a = 1
        # self.curve_val_b = 3
        self.alphabet: dict = {}

    def is_on_curve(self, point):
        """
            **Determines if the point is on the curve. We assume that the coordinates x and y are always
            modulo p, so that we can compare two points for equality with a simple ==.**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point: Point(x, y)
                :type point: Point

            Returns:
                :return: true if the point is on the curve, False otherwise.
                :rtype: Boolean
        """
        if point == self.origin:
            return True
        return (
            (point.y**2 - (point.x**3 + self.curve_val_a*point.x + self.curve_val_b)) % self.mod == 0 and
            0 <= point.x < self.mod and 0 <= point.y < self.mod)

    def find_mod_inverse(self, val):
        """
            **Function that returns the inverse of val modulo mod, considering that
            x is not divisible by self.mod**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param val: Value to be reversed
                :type val: int

            Returns:
                :return: Returns the inverse of val modulo mod
                :rtype: int
            .. note:: Another method is possible to calculate the inverse, it is described below.
                    It is to go back up the Euclid algorithm.
            .. seealso:: def find_mod_inverse_euclidean(val, mod) line 128
            .. _foo:
        """
        if val % self.mod == 0:
            raise ZeroDivisionError("inverse not possible")
        return pow(val, self.mod-2, self.mod)

    @staticmethod
    def pgcd(val_a, val_b):
        """
            **Calculates the largest common divisor of a and b.
            Unless b == 0, the result will have the same sign as b (so when
            b is divided by it, the result is positive).**

            Args:
                :param val_a: Number 1
                :type val_a: int

                :param val_b: Number 2
                :type val_b: int

            Returns:
                :return: PGCD of val_a and val_b
                :rtype: int
        """
        while val_b:
            val_a, val_b = val_b, val_a % val_b
        return val_a

    def find_mod_inverse_euclidean(self, val, mod):
        """
            foo_
            **Function that returns the inverse of val modulo mod. It is the number x tq
            val * x % mod = 1**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param val: Value to be inverted
                :type val: int

                :param mod: Modulo (order of the elliptic curve)
                :type mod: int

            Returns:
                :return: Returns val modulo mod
                :rtype: int
            .. note:: This algorithm exploits Euclid's algorithm with the Bézout coefficients.
        """
        if self.pgcd(val, mod) != 1:
            raise Exception("The modular reverse doesn't exist")  # no modular reverse if a & m not prime between them

        # Calculation using the extended euclide algo:
        u_1, u_2, u_3 = 1, 0, val
        v_1, v_2, v_3 = 0, 1, mod
        while v_3 != 0:
            quotient = u_3 // v_3  # //  division with remainder operator
            v_1, v_2, v_3 = (u_1 - quotient * v_1), (u_2 - quotient * v_2), (u_3 - quotient * v_3)
            u_1, u_2, u_3 = v_1, v_2, v_3
        return u_1 % mod

    def opposite(self, point):
        """
            **Returns the inverse of point P on the elliptic curve y^2 = x^3 + ax + b.**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point: Point(x, y)
                :type point: Point

            Returns:
                :return: Returns the inverse of the point P on the elliptic curve y^2 = x^3 + ax + b.
                :rtype: Point
        """
        if point == self.origin:
            return point
        return self.point(point.x, (-point.y) % self.mod)

    def double_point(self, point):
        """
            **Function that returns the sum P + P on the elliptic curve y^2 = x^3 + ax + b.**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point: Point(x, y)
                :type point: Point

            Returns:
                :return: 2 * Point(x, y)
                :rtype: Point
        """
        if not self.is_on_curve(point):
            raise ValueError("The point isn't on the curve")
        if point == self.origin:
            return self.origin
        if point.y == 0:
            return self.origin
        delta = (3 * point.x**2 + self.curve_val_a) * self.find_mod_inverse(2 * point.y)
        val_x = (delta**2 - 2 * point.x) % self.mod
        val_y = ((delta * point.x - delta * val_x) - point.y) % self.mod
        result = self.point(val_x, val_y)

        # The point must be on the curve
        assert self.is_on_curve(result)
        return result

    def p_sum_q(self, point_a, point_b):
        """
            **Sum of the points P and Q on the elliptic curve y^2 = x^3 + ax + b.**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point_a: Point_a(xA, yA)
                :type point_a: Point

                :param point_b: Point_b(xB, yB)
                :type point_b: Point

            returns:
                :return: Sum of Point_a(xA, yA) + Point_b(xB, yB)
                :rtype: Point
        """
        if not (self.is_on_curve(point_a) and self.is_on_curve(point_b)):
            raise ValueError("Les points ne sont pas sur la courbe")

        # Cas particulier si P ou Q ou ou P + Q est le point à l'origine.
        if point_a == self.origin:
            result = point_b
        elif point_b == self.origin:
            result = point_a
        elif point_b == self.opposite(point_a):
            result = self.origin
        elif point_b.x - point_a.x == 0:
            result = self.origin
        else:
            delta = (point_b.y - point_a.y) * \
                self.find_mod_inverse(point_b.x - point_a.x)
            val_x = (delta**2 - point_a.x - point_b.x) % self.mod
            val_y = (delta * (point_a.x - val_x) - point_a.y) % self.mod
            result = self.point(val_x, val_y)

        # On doit obtenir un autre point sur la courbe.
        assert self.is_on_curve(result)
        return result

    def fast_exp(self, point, factor):
        """
            **Function that returns a point that is worth factor * Point**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point: Point(x, y) to be multiplied
                :type point: Point

                :param factor: Multiplication factor
                :type factor: int

            Returns:
                return: Point(x, y) * factor
                :rtype: Point
        """
        if factor == 0:
            return self.origin
        if factor == 1:
            return point
        if factor % 2 == 1:
            # addition quand factor est impair
            return self.p_sum_q(point, self.fast_exp(point, factor - 1))
        # point + point quand factor est pair
        return self.fast_exp(self.double_point(point), factor / 2)

    def generate_private_key(self, num_bits):
        """
            **Private key generation**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param num_bits: Number of bits
                :type num_bits: int

            Returns:
                :return: Private key
                :rtype: int
            .. note:: The private key is random.
        """
        return int.from_bytes(os.urandom(num_bits), sys.byteorder) % self.mod

    def send_diffie_hellman(self, private_key, public_generator):
        """
            **Send encryption key**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param private_key: Private key
                :type private_key: int

                :param public_generator: Generator point
                :type public_generator: Point

            Returns:
                :return: Public key
                :rtype: Point
            .. note:: The generating point is public
        """
        return self.fast_exp(public_generator, private_key)

    def receive_diffie_hellman(self, private_key, public_key):
        """
            Receipt of encryption key

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param private_key: private key
                :type private_key: int

                :param public_key: public key
                :type public_key: int

            Returns:
                :return:
                :rtype:
        """
        return self.fast_exp(public_key, private_key)

    def encode_ascii_message(self, num_bits):
        """
            **Encoding of an ASCII text as points on the curve**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param num_bits: Number of bits of the random number fixing the abcissa of the chosen point.
                :type num_bits: int

            Returns:
                :return: Dictionary that associates an ASCII character with a point of the curve.
                :rtype: dictionary
        """
        # Set of ciphered pairs
        for i in range(0, len(string.ascii_lowercase)):
            val_x = int.from_bytes(os.urandom(num_bits), sys.byteorder) % self.mod
            val_y = (math.sqrt(val_x**3 + self.curve_val_a*val_x + self.curve_val_b)) % self.mod
            while (not val_y.is_integer()
                   or not self.is_on_curve(self.point(val_x, val_y))
                   or self.point(val_x, val_y) in self.alphabet.values()):
                print(self.point(val_x, val_y))
                val_x = int.from_bytes(os.urandom(num_bits), sys.byteorder) % self.mod
                val_y = (math.sqrt(val_x**3 + self.curve_val_a*val_x + self.curve_val_b)) % self.mod
            point = self.point(val_x, val_y)
            self.alphabet[string.ascii_lowercase[i]] = point
        return self.alphabet

    # Encrypt a point_to_cipher using the public key k

    def cipher(self, point, public_key_dest, private_key_sender, point_to_cipher):
        """
            **Encryption of a point**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param point: Shared key
                :type point: Point

                :param public_key_dest: Recipient's public key
                :type public_key_dest: Point

                :param private_key_sender: Private key of the transmitter
                :type private_key_sender: int

                :param point_to_cipher: Point to encrypt
                :type point_to_cipher: Point

            Returns:
                :return: Point encrypted with C1 and C2
                :rtype: Point
        """
        val_c1 = self.fast_exp(point, private_key_sender)
        val_c2 = self.p_sum_q(point_to_cipher, self.fast_exp(public_key_dest, private_key_sender))
        if not self.is_on_curve(val_c1) or not self.is_on_curve(val_c2):
            raise ValueError("Ciphered points aren't on the curve.")
        return val_c1, val_c2

    # Message deciphered is given by M = C2 - (privKeyDest)*C1

    def decipher(self, private_key_dest, val_c1, val_c2):
        """
            **Deciphering a point**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param private_key_dest: Recipient's private key
                :type private_key_dest: int

                :param val_c1: C1 = private key of the transmitter * shared point
                :type val_c1: Point

                :param val_c2: C2 = Point to be encrypted + private key sender * public key receiver
                :type val_c2: Point

            Returns:
                :return: Point deciphered
                :rtype: Point
        """
        return self.p_sum_q(val_c2, self.opposite(self.fast_exp(val_c1, private_key_dest)))

    def random_public_point(self, mod, num_bits):
        """
            **Generation of a random point on the curve**

            Args:
                :param self: Pending instance
                :type self: Utils object

                :param mod: Order of the selected elliptic curve
                :type mod: int

                :param num_bits: Value which allows to set randomly the value of x
                :type num_bits: int

            Returns:
                :return: Point on the elliptic curve
                :rtype: Point
        """
        val_x = int.from_bytes(os.urandom(num_bits), sys.byteorder) % mod
        val_y = (math.sqrt(val_x**3 + self.curve_val_a*val_x + self.curve_val_b)) % mod
        while not val_y.is_integer() or not self.is_on_curve(self.point(val_x, val_y)):
            val_x = int.from_bytes(os.urandom(num_bits), sys.byteorder) % mod
            val_y = (math.sqrt(val_x**3 + self.curve_val_a*val_x + self.curve_val_b)) % mod
        return self.point(val_x, val_y)
