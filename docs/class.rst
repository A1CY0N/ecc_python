Main class
**********

.. automodule:: elliptic_curves

.. autoclass:: Ecc
    :members: __init__
    :private-members:
    :special-members:
