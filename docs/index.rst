.. TP_Elliptic_curves_Python documentation master file, created by
   sphinx-quickstart on Thu Mar 22 13:32:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Project documentation
=====================
This project is an implementation of El-Gamal encryption on elliptic curves. 
To learn more, you can consult the `wikipedia page`__ that explains the El gamal encryption.

The project uses version 3 of python

.. __: https://en.wikipedia.org/wiki/ElGamal_encryption

.. warning::
   This project was created only to learn El gamal encryption on 
   elliptic curves. It is not a safe implementation to use in other projects. 
   

   **It should only be used for learning or testing purposes.**

.. toctree::
   :maxdepth: 2

   fonctions
   class



Index of documentation
======================

* :ref:`genindex`
* :ref:`search`
