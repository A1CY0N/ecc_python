# ECC_python

> :warning: **If you are using this project**: This project was created only to learn El gamal encryption on elliptic curves. It is not a safe implementation to use in other projects. **It should only be used for learning or testing purposes.**

## Description
Python project during the crypto course :
- Implementation of an elliptic curve
- Implementation of a key exchange via Diffie-Hellman
- Implementation of an asymmetrical encryption by El-Gamal


## Requirements

* Python 3.x
* Libraries
  * sphinx_bootstrap_theme
  * flake8
  * Pylint
  * Mypy
  * Sphinx

Check ``requirements.txt`` for complete list


## Documentation
You can consult the project documentation [here](https://a1cy0n.gitlab.io/ecc_python/).

## Meta

[@A1CY0N](https://mamot.fr/@a1c0n) – a1cy0n@tutanota.com

Distributed under the MIT license. See ``LICENSE`` for more information.

[https://gitlab.com/A1CY0N](https://gitlab.com/A1CY0N)

## Contributing

1. Fork it (<https://gitlab.com/A1CY0N/ecc_python>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request